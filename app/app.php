<?php
require_once __DIR__.'/../vendor/autoload.php';
$app = new Silex\Application();

require_once __DIR__. '/config.php';

$app['debug'] = true;

//@BEGIN dbconnection
$db_config['db_name'] = 'igindex';
$db_config['db_user'] = 'root';
$db_config['db_password'] = ':H7/Ca44dt';
$db_config['db_host'] = 'localhost';
//@END dbconnection

//@BEGIN IgIndex settings
$app['api_url'] = 'https://demo-api.ig.com/gateway/deal/';
$app['ig_api_key'] = 'e59e159780a66aab57e167aeec45863faa40c244';
$app['ig_user'] = 'christocmpdemo';
$app['ig_password'] = ':H7/Ca44dt';
$app['ig_api_version'] = 3;
//@END IgIndex settings

//@BEGIN live IgIndex settings
$app['ig_live_api_key'] = 'b41ee9b8e443c1a2f69b71bde1410e3cf8a12634';


$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
'db.options' => array(
'dbname' => $db_config['db_name'],
'user' => $db_config['db_user'],
'password' => $db_config['db_password'],
'host' => $db_config['db_host'],
'driver' => 'pdo_mysql',
),
));


//@BEGIN Debug mode conditionals
if ($app['debug']) {
    $app->register(new Silex\Provider\MonologServiceProvider(), array(
        'monolog.logfile' => __DIR__ . '/development.log',
    ));

    $app['monolog'] = $app->share($app->extend('monolog', function ($monolog, $app) {
        //$monolog->pushHandler(new Monolog\Handler\StreamHandler('path/to/your.log', Monolog\Logger::WARNING));
        $monolog->pushHandler(new Monolog\Handler\BrowserConsoleHandler());
        $monolog->pushProcessor(new Monolog\Processor\IntrospectionProcessor()); //gives line number
        $monolog->pushProcessor(new Monolog\Processor\WebProcessor()); // client call details
        $monolog->pushProcessor(new Monolog\Processor\MemoryUsageProcessor());
        //todo look into utility classes e.g behaviour when there is a lot of error logging
        return $monolog;
    }));
}
//@END Debug mode conditionals

$app->register(new Silex\Provider\TwigServiceProvider(), array(
'twig.path' => __DIR__.'/../web/views',
));

//$app['monolog']->addDebug('before app view called');
$app->view(function (array $controllerResult, Request $request) use ($app) {
    //$app['monolog']->addDebug('app view called');
    return $app->json($controllerResult."7");
});
//$app['monolog']->addDebug('after app view called');



//Controllers
$app->register(new Silex\Provider\ServiceControllerServiceProvider());

$app->mount('/', include  __DIR__.'/../controllers/page.php');
$app->mount('/blog', include  __DIR__.'/../controllers/blog.php');
$app->mount('/igindexapi', include  __DIR__.'/../controllers/igindexRequest.php');


return $app;
//Middlewares
/*
$app->before(function () {
    echo "Silex start event fired.\n";
});

$app->before(function (\Symfony\Component\HttpFoundation\Request $request, Silex\Application $app) {
    print_r ($request->request->keys());
});
*/