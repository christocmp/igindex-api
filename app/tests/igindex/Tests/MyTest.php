<?php
require_once __DIR__.'/../../../../vendor/autoload.php';

use Silex\WebTestCase;

class YourTest extends WebTestCase
{

public function setUp() {
    parent::setUp();
    return $this->createApplication();
}

public function createApplication()
{
    $app = require __DIR__.'/../../../app.php';
    unset($app['exception_handler']);
    return $app;
}



public function testFooBar()
{
    //return true;


    $client = $this->createClient();
    $crawler = $client->request('GET', '/');

    $this->assertTrue($client->getResponse()->isOk());
    $this->assertCount(1, $crawler->filter('h1:contains("Contact us")'));
    $this->assertCount(1, $crawler->filter('form'));

}
}

?>