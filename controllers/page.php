<?php

$page = $app['controllers_factory'];

$page->get('/', function () use ($app) {
    $name = 'christo';
    //   $test_url = 'https://demo-api.ig.com/gateway/deal/prices/IX.D.FTSE.DAILY.IP?resolution=MINUTE_10&from=2015-07-21T00%3A00%3A00&to=2015-07-21T00%3A40%3A00';
    $test_urls = array();
    http://igindex.app/igindexapi/prices/ftse100/minute_10/2015-06-01%2009:00:00/2015-06-01%2019:30:00
    $test_urls[] = array('href' => '/igindexapi/prices/ftse100/minute_10/2015-06-01 09:00:00/2015-06-01 09:30:00', 'caption' => 'FTSE Price (cache)');
    $test_urls[] = array('href' => '/igindexapi/prices/ftse100/minute_10/2016-02-16T09:00:00/2016-02-16T09:10:00', 'caption' => 'FTSE Price (no cache)');
    $test_urls[] = array('href' => '/igindexapi/prices/ftse100/MINUTE_10/2015-03-11 08:00:00/2015-03-14 09:00:00', 'caption' => 'FTSE Price - URL from API test');
   // $url_parameters2 = 'IX.D.FTSE.DAILY.IP/MINUTE_10/2015-06-11%2008%3A00%3A00/2015-06-11%2009%3A00%3A00';
    return $app['twig']->render('debug/index.twig', array(
        'navigation' => $test_urls,
    ));
});

return $page;

