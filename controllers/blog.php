<?php
// blog.php

$blogPosts = array(
    1 => array(
        'date'      => '2015-03-29',
        'author'    => 'igorw',
        'title'     => 'Using Silex',
        'body'      => '...',
    ),
    2 => array(
        'date'      => '2015-04-03',
        'author'    => 'igorw',
        'title'     => 'Using Silex (Part 2)',
        'body'      => '...',
    ),
);

$blog = $app['controllers_factory'];

$idChecker = function ($id){
    return intval($id);
};

$app->get('/blog', function () use ($blogPosts) {
    $output = '';
    foreach ($blogPosts as $post) {
        $output .= $post['title'];
        $output .= '<br />';
    }

    return $output;
});

$blog->get('/', function () { return 'Blog home page (in controller dir)'; });

$app->get('/blog/{id}', function (Silex\Application $app, $id) use ($blogPosts) {
    if (!isset($blogPosts[$id])) {
        $app->abort(404, "Post $id does not exist.");
    }

    $post = $blogPosts[$id];

    return  "<h1>$id</h1>
<h1>{$post['title']}</h1>".
    "<p>{$post['body']}</p>";
})->convert('id', $idChecker);


$app->get('/blog2/{id}', function (Silex\Application $app, \Symfony\Component\HttpFoundation\Request $request, $id) use ($blogPosts) {
    $output = '';
    foreach ($blogPosts as $post) {
        $output .= $post['title'];
        $output .= '<br />';
    }
    $output .= $request->get('search');
    $output .= $request->get('searchtype');

    return $output;
});


return $blog;