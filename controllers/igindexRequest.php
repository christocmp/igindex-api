<?php
$igindex_request = $app['controllers_factory'];
require_once __DIR__.'/../models/apidata.php';

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

$client = new Client([
    'base_uri' => $app['api_url'],
    'timeout'  => 2.0,
]);

//load require models
$app['apidatamodel'] = function () use ($app) {
    return new apidata($app); // inject the app on initialization
};

//helper functions
$app['api_authenticate'] = function () use ($app, $client) {


    if ( ! isset($app['ig_authentication_time'] )){ //are we authenticated
        $app['ig_authentication_time'] = $app['apidatamodel']->getIgAuthenticationTime(); //try db cache
    }

    if ($app['ig_authentication_time'] == 0 || ($app['ig_authentication_time'] <= (time() -  600))){
        echo '<h1>tokens are too old</h1>';
    }
    else{ //tokens are fresh enough to use from cache
        echo '<h1>Using cached tokens</h1>';
        $app['ig_session_token']  = $app['apidatamodel']->getIgSessionToken();
        $app['ig_security_token'] = $app['apidatamodel']->getIgSecurityToken();
        return;
    }
    echo '<h1>getting new keys</h1>';
    //build request body and send
    $headers  = ['Content-Type' => 'application/json; charset=UTF-8', 'Accept' => 'application/json; charset=UTF-8', 'X-IG-API-KEY' => 'e59e159780a66aab57e167aeec45863faa40c244', 'Version' => '1'];
    $body = json_encode(array('identifier' => 'christocmpdemo', 'password' => ':H7/Ca44dt'));

    $request = new Request('POST', $app['api_url'] . 'session', $headers, $body);

    $response = $client->send($request);

    //refresh the age of tokens in app
    $app['ig_authentication_time'] = time();
    $app['apidatamodel']->setIgAuthenticationTime($app['ig_authentication_time']);

    if ($response->hasHeader('CST')) {
        $app['ig_session_token'] = $response->getHeader('CST')[0];
        $app['apidatamodel']->setIgSessionToken($response->getHeader('CST')[0]);
    }

    if ($response->hasHeader('X-SECURITY-TOKEN')) {
        $app['ig_security_token'] = $response->getHeader('X-SECURITY-TOKEN')[0];
        $app['apidatamodel']->setIgSecurityToken($response->getHeader('X-SECURITY-TOKEN')[0]);
    }
};

// callback
$toUppercase = function ($data_type, \Symfony\Component\HttpFoundation\Request $request) {
    return strtoupper($data_type);
};

//return price data - this will be the meat of the api
$igindex_request->get('/prices/{epic_short_name}/{data_type}/{from}/{to}', function (Silex\Application $app, $epic_short_name, $data_type, $from, $to){


    //get epic id from easy url shortname
    $epic_id = $app['apidatamodel']->getEpicIdFromShortName($epic_short_name)->id;

    if ($app['debug']) {
        if (!$epic_id) {
            $app['monolog']->addDebug('Epic ID returned false');
        } else {
            $app['monolog']->addDebug('Epic ID = ' . $epic_id);
        }
    }

    //try cache look up first
    $cached_prices = $app['apidatamodel']->getCachedPriceRecord($epic_id, $data_type, $epic_id, $from, $to);


    //check cache lookup has full range
    //todo  - allow partial cache lookup
    /*
     * For now we check between dates and perform a full api lookup if *any* dates are missing
     * 9/10 this is fine in the current usage case. A lookup for just the missing data
     * would be better
     */

    //work expected number of results
    if (strpos (strtoupper($data_type), 'MINUTE') !== false) {
        $exploded = explode ('_', $data_type, 2);
        $time_factor = $exploded[1];
    }

    else{ // non minute period
        switch (strtoupper($data_type)) {
            case 'DAILY':
                $time_factor = 1440;
                break;
            case 'WEEKLY':
                $time_factor = 10080;
                break;
            case 'MONTHLY':
                $time_factor = 40320;
                break;
        }
    }



    $datediff = strtotime($to) - strtotime($from);
    $expected_results = floor($datediff/(60*$time_factor) + 1);  // add 1 to count first result
    $actual_results = count($cached_prices);

    //verify full cache hit
    if (count($cached_prices) > 0) {
        echo '<h1>Cache hit - verifying full cache hit<h1>';
        foreach ($cached_prices as $cached_price) {
            echo $cached_price['time_period'] . '<br />';
            echo $cached_price['open_bid'] . '<br />';
        }
    }

    echo '<h1>Expected results:' . $expected_results . '</h1>';
    echo '<h1>Actual results:' . $actual_results . '</h1>';


    if ( (count($cached_prices) == 0) || ($expected_results != $actual_results)) {

        //empty cache
        if ((count($cached_prices) == 0)){
            echo '<h1>cache empty</h1>';
            //die();
            if ($app['debug']) {
                $app['monolog']->addDebug('No cached prices');
            }

            $api_prices = $app['apidatamodel']->getIgPriceRecord($epic_id, $data_type, $epic_id, $from, $to); //returns json decoded

            foreach($api_prices->prices as $api_price){
                print_r ($api_price);
                echo '<h1>' .$api_price->snapshotTime. '</h1>';
                $app['apidatamodel']->saveIgPriceRecord($epic_id, $data_type, $epic_id,$api_price); //$epic_id, $data_type, $data_source_id, $record
            }
            //print_r ($api_prices);

        }

        //partial cache hit
        if ($expected_results != $actual_results) {

            //determine data sets needed to be checked

            $plot_timestamp = strtotime($from);

            for ($i = 0; ($i < $expected_results); $i++){
                echo 'data point ' .$i;
                echo 'time plot ' . ($plot_timestamp + ($i * ($time_factor*60)));

                $plot_time = date('Y-m-d\TH:i:s', ($plot_timestamp + ($i * ($time_factor*60))));
                echo $plot_time;

                //try cache look up first

                $cached_price = $app['apidatamodel']->getCachedPriceRecord($epic_id, $data_type, $epic_id, $plot_time, $plot_time);
                if (count($cached_price) < 1){
                    echo 'no cached prices';
                    $api_prices = $app['apidatamodel']->getIgPriceRecord($epic_id, $data_type, $epic_id, $plot_time, $plot_time);
                    echo 'getting api price <hr />';

                    foreach($api_prices->prices as $api_price){
                        echo '<h1>' .$api_price->snapshotTime. '</h1>';
                        $app['apidatamodel']->saveIgPriceRecord($epic_id, $data_type, $epic_id,$api_price); //$epic_id, $data_type, $data_source_id, $record
                        print_r($api_price);
                    }

                    die();
                }

                echo '<br />';
            }

            foreach ($cached_prices as $cached_price){
                print_r ($cached_price);
            }




            echo '<h1>partial cache hit</h1>';

            die();


        }
        die();


    }





    //$app['monolog']->addDebug(print_r($cached_prices, true));
    //$app['apidatamodel']->saveIgPriceRecord(1,'MINUTE_10',2,$record); //$epic_id, $data_type, $data_source_id, $record


    $output = '<h1>API Call variables:</h1>';
    $output .= 'epic short name:' . $epic_short_name .'<br />';
    $output .= 'data type:' . $data_type .'<br />';
    $output .= 'from:' . $from .'<br />';
    $output .= 'to:' . $to .'<br />';

    return $output;
})->convert("data_type", $toUppercase);


//query available epics - an epic is the financial instrument ftse, oil price etc
$igindex_request->get('/epic/search/{epic_short_name}', function (Silex\Application $app, $epic_short_name)  {

    $data = $app['apidatamodel']->queryEpicIds($epic_short_name);

    print_r($data);

    return false;


});

return $igindex_request;