<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;


/**
 * Class Connection
 *
 * Basic connection class using singleton design pattern
 */
class apidata

{

    private $_app;
    private $_client;

    public function __construct(Silex\Application $app)
    {
        $this->_app = $app;
        $this->_client = new Client([
            'base_uri' => $this->_app['api_url'],
            'timeout'  => 2.0,
        ]);

    }

    public function __destruct()
    {

    }

    /**
     * @return Connection
     *
     * returns a new connection object or returns existing one
     */

    public function get()
    {

            $sql = "SELECT * FROM api_cache_price";
            $post = $this->_app['db']->fetchAssoc($sql);

        return $post;

    }

    /**
     * @return Connection
     *
     * returns a new connection object or returns existing one
     */

    public function getIgAuthenticationTime()
    {
        $sql = "SELECT value FROM api_tokens WHERE name = 'IG_AUTHENTICATION_TIME' LIMIT 1";
        $token = $this->_app['db']->fetchAssoc($sql);

        return $token['value'];
    }

    /**
     * @return Connection
     *
     * returns a new connection object or returns existing one
     */

    public function getIgSessionToken()
    {
        $sql = "SELECT value FROM api_tokens WHERE name = 'IG_SESSION_TOKEN' LIMIT 1";
        $token = $this->_app['db']->fetchAssoc($sql);

        return $token['value'];
    }

    /**
     * @return Connection
     *
     * returns a new connection object or returns existing one
     */

    public function getIgSecurityToken()
    {
        $sql = "SELECT value FROM api_tokens WHERE name = 'IG_SECURITY_TOKEN' LIMIT 1";
        $token = $this->_app['db']->fetchAssoc($sql);

        return $token['value'];
    }

    /**
     * @return Connection
     *
     * returns a new connection object or returns existing one
     */

    public function setIgSessionToken($value)
    {
        $sql = "UPDATE api_tokens SET value = ? WHERE name = ? LIMIT 1";
        $this->_app['db']->executeUpdate($sql, array($value, 'IG_SESSION_TOKEN'));
    }

    /**
     * @return Connection
     *
     * returns a new connection object or returns existing one
     */

    public function setIgSecurityToken($value)
    {
        $sql = "UPDATE api_tokens SET value = ? WHERE name = ? LIMIT 1";
        $this->_app['db']->executeUpdate($sql, array($value, 'IG_SECURITY_TOKEN'));
    }

    /**
     * @return Connection
     *
     * returns a new connection object or returns existing one
     */

    public function setIgAuthenticationTime($value)
    {
        $sql = "UPDATE api_tokens SET value = ? WHERE name = ? LIMIT 1";
        $this->_app['db']->executeUpdate($sql, array($value, 'IG_AUTHENTICATION_TIME'));
    }

    public function saveIgPriceRecord($epic_id, $data_type, $data_source_id, $record){
        $queryBuilder = $this->_app['db']->createQueryBuilder();
        $queryBuilder->insert('api_cache_price')
            ->values(
                array(
                    'epic_id' => '?',
                    'datatype' => '?',
                    'data_source_id' => '?',
                    'time_period' => '?',
                    'open_bid' => '?',
                    'open_ask' => '?',
                    'open_lasttraded' => '?',
                    'close_bid' => '?',
                    'close_ask' => '?',
                    'close_lasttraded' => '?',
                    'high_bid' => '?',
                    'high_ask' => '?',
                    'high_lasttraded' => '?',
                    'low_bid' => '?',
                    'low_ask' => '?',
                    'low_lasttraded' => '?',
                    'last_traded_volume' => '?'
                 )
            )
            ->setParameter(0, $epic_id)
            ->setParameter(1, $data_type)
            ->setParameter(2, $data_source_id)
            ->setParameter(3, $record->snapshotTime)

            ->setParameter(4, $record->openPrice->bid)
            ->setParameter(5, $record->openPrice->ask)
            ->setParameter(6, $record->openPrice->lastTraded)

            ->setParameter(7, $record->closePrice->bid)
            ->setParameter(8,  $record->closePrice->ask)
            ->setParameter(9,  $record->closePrice->lastTraded)

            ->setParameter(10,  $record->highPrice->bid)
            ->setParameter(11,  $record->highPrice->ask)
            ->setParameter(12,  $record->highPrice->lastTraded)

            ->setParameter(13,  $record->lowPrice->bid)
            ->setParameter(14,  $record->lowPrice->ask)
            ->setParameter(15,  $record->lowPrice->lastTraded)

            ->setParameter(16,  $record->lastTradedVolume);
        $queryBuilder->execute();
    }


    // tries to get record from cache returns false otherwise
    public function getCachedPriceRecord($epic_id, $data_type, $data_source_id, $from, $to)
    {

        $queryBuilder = $this->_app['db']->createQueryBuilder();

     /*   $queryBuilder->select('*' )
            ->from('api_cache_price')
            ->where(
                $queryBuilder->expr()->andX(
                    $queryBuilder->expr()->eq('datatype', '?'),
                    $queryBuilder->expr()->eq('epic_id', '?'),
                    $queryBuilder->expr()->eq('data_source_id', '?'),
                    $queryBuilder->expr()->gte('time_period', '?'),
                    $queryBuilder->expr()->lte('time_period', '?')
                )

            )
            ->setParameter(0, $data_type)
            ->setParameter(1, $epic_id)
            ->setParameter(2, '2')
            ->setParameter(3, strtotime($from))
            ->setParameter(4, strtotime($to));

       */

    /*    echo 'date we are getting for from:';
        echo date("Y-m-d H:i:s", strtotime($from));
        die();   */
        $queryBuilder->select('*' )
            ->from('api_cache_price')
            ->where('epic_id = :epic_id AND datatype = :data_type AND
            time_period >= :time_period_from AND time_period <= :time_period_to')
            ->setParameter('epic_id', $epic_id)
            ->setParameter('data_type', $data_type)
            ->setParameter('time_period_from', date("Y-m-d H:i:s", strtotime($from)))
            ->setParameter('time_period_to', date("Y-m-d H:i:s", strtotime($to)));

        echo 'from' . date("Y-m-d H:i:s", strtotime($from));

        echo 'to' . date("Y-m-d H:i:s", strtotime($to));

        return $queryBuilder->execute()->fetchAll();

    }

    //gets price record from remote API
    public function getIgPriceRecord($epic_id, $data_type, $data_source_id, $from, $to)
    {

        $this->_app['api_authenticate'];

        //build request body and send
        $headers  = [
            'Content-Type' => 'application/json; charset=UTF-8',
            'Accept' => 'application/json; charset=UTF-8',
            'X-IG-API-KEY' => $this->_app['ig_api_key'],
            'version' => $this->_app['ig_api_version'],
            'X-SECURITY-TOKEN' => $this->_app['ig_security_token'],
            'CST' => $this->_app['ig_session_token']
        ];

        //$body = json_encode(array('identifier' => 'christocmpdemo', 'password' => ':H7/Ca44dt'));
        //IX.D.FTSE.DAILY.IP/MINUTE_10/2015-06-11%2008%3A00%3A00/2015-06-11%2009%3A00%3A00

        //?resolution=MINUTE_10&from=2015-07-21T00%3A00%3A00&to=2015-07-21T00%3A30%3A00

        $url_parameters = $this->getEpic($epic_id)->name;
        $url_parameters .= '?resolution=' .strtoupper($data_type);
        $url_parameters .= '&from=' .urlencode($from);
        $url_parameters .= '&to=' .urlencode($to);

        echo '<hr />';
        echo 'url parameters: <br />';
        echo '<br />' . $this->_app['api_url'] . 'prices/' .$url_parameters;
        echo '<hr />';

        $request = new Request('GET', $this->_app['api_url'] . 'prices/' .$url_parameters, $headers);

        $response = $this->_client->send($request);


        echo '<h1>after ig get</h1>';
        return json_decode($response->getBody());


    }


    // returns a list of available epics for use when epic is not known
    public function queryEpicIds($shortname){
        $queryBuilder = $this->_app['db']->createQueryBuilder();
        $queryBuilder->select('id', 'name', 'short_name', 'description', 'timestamp' )
            ->from('epic')
            ->where('short_name = :shortname')
            ->setParameter('shortname', $shortname);
        return $queryBuilder->execute()->fetchAll();

       // return $queryBuilder->getResult();
    }


    public function getEpicIdFromShortName($shortname){
        $queryBuilder = $this->_app['db']->createQueryBuilder();
        $queryBuilder->select('id')
            ->from('epic')
            ->where('short_name = :shortname')
            ->setMaxResults(1)
            ->setParameter('shortname', $shortname);
        return $queryBuilder->execute()->fetch(PDO::FETCH_OBJ); //will return false on no records
    }

    public function getEpic($id){
        $queryBuilder = $this->_app['db']->createQueryBuilder();
        $queryBuilder->select('*')
            ->from('epic')
            ->where('id = :id')
            ->setMaxResults(1)
            ->setParameter('id', $id);
        return $queryBuilder->execute()->fetch(PDO::FETCH_OBJ); //will return false on no records
    }

}